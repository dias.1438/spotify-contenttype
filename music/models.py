from django.conf import settings
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


User = settings.AUTH_USER_MODEL

class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=80)

    content_type = models.ForeignKey(
        ContentType, 
        default=None, 
        null=True, 
        blank=True, 
        on_delete=models.SET_NULL
        )
    object_id = models.PositiveBigIntegerField(null=True, blank=True, default=None)
    item = GenericForeignKey("content_type", "object_id")

    # class Meta:
    #     indexes = [
    #         models.Index(fields=["content_type", "object_id"]),
    #     ]

    def __str__(self):
        return f"{self.user} - {self.content_type}"


class Artist(models.Model):
    name = models.CharField(max_length=80)

    favorite = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class Album(models.Model):
    title = models.CharField(max_length=80)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    release_date = models.DateField(null=True, blank=True)

    favorite = models.ManyToManyField(User)

    def __str__(self):
        return f"{self.title}"


class Song(models.Model):
    title = models.CharField(max_length=80)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)

    favorite = models.ManyToManyField(User)

    favorite = GenericRelation(Favorite, related_query_name="song")

    def __str__(self):
        return f"{self.title}"
