from django.contrib import admin

from music.models import Artist, Album, Song, Favorite

# Register your models here.
class ArtistAdmin(admin.ModelAdmin):
    list_display = ("name",)

admin.site.register(Artist, ArtistAdmin)

class AlbumAdmin(admin.ModelAdmin):
    list_display = ("title", "artist", "release_date")

admin.site.register(Album, AlbumAdmin)

class SongAdmin(admin.ModelAdmin):
    list_display = ("title", "artist", "album")
admin.site.register(Song, SongAdmin)

class FavoriteAdmin(admin.ModelAdmin):
    list_display = ("user", "title", "content_type", "object_id", "item")
admin.site.register(Favorite, FavoriteAdmin)